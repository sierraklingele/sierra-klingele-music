# Website source for sierraklingele.com

## to run @ [localhost:8080](http://localhost:8080)
`npx @11ty/eleventy --serve`

## to stop
`Ctrl+c`

## to build the public content
`npx @11ty/eleventy`

1. build logos and design
  - ✓ find a nice base css library
  - style with colors and fonts

2. build website
  - ✓ codeberg
  - ✓ core page layout / sitemap
  - ✓ support site index at top of every page
    - decide what pages I want to have (about, contact, etc.)
  - ✓ events should be generated from single files containing tags, date, and title
  - ✓ index is list of all root pages dynamically generated, vertical plain list
  - style list items so they don't have bullet point

3. ✓ renew domain

4. find a hosting service
  - ✓ cancel bluehost
  - ✓ surge.sh, free static site hosting

5. embed soundcloud links