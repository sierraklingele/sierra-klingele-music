---
layout: tab.liquid
title: Events
tags: tab
---
<ul>
    {% for item in collections.event -%}
        <li>
            <h4>{{ item.data.date | date: '%B %d, %Y' }} - {{ item.data.title }}</h4>
            {{ item.content }}
        </li>
        <hr>
    {% endfor %}
</ul>
