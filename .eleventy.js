module.exports = function (eleventyConfig) {
    eleventyConfig.addPassthroughCopy("./src/*.css")
    eleventyConfig.addPassthroughCopy("./src/*.otf")
    return {
        dir: {
            input: "src",
            output: "public",
        },
    };
};
